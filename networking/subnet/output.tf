output "subnet_name" {
  description = "The name of the newly created subnet"
  value = google_compute_subnetwork.subnet.name
}