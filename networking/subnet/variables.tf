# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED MODULE PARAMETERS
# These parameters must be supplied when consuming this module.
# ---------------------------------------------------------------------------------------------------------------------

variable "project_id" {
  description = "project id where the vpc and subnet reside"
}

variable "vpc_name" {
  description = "VPC name where the subnet is going to reside"
}

variable "region" {
  description = "region that the project is built in"
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL MODULE PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "cidr_range" {
  description  = "The CIDR address range to be used with this given subnet"
  default = "10.10.0.0/24"
}
