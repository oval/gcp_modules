# VPC Subnet module #

This module creates a new subnet in the given VPC. There is an optional CIDR range variable if you are creating multiple subnets.

## Variables ##

### Required ###

- Project - string
- VPC name - string
- Region - string

### Optional ###

- CIDR range - string in format ```\d{2,3}:\d{1,3}:\d{1,3}:\d{1,3}/(8|16|24|32)```

## Outputs ##

- Subnet name - string
