# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED MODULE PARAMETERS
# These parameters must be supplied when consuming this module.
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "project where the vpc reside"
}

variable "project_id" {
  description = "project id where the vpc reside"
}

variable "region" {
  description = "region where the project is built"
}