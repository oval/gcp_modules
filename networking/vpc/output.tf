output "vpc_name" {
  description = "The name of the newly created VPC"
  value       = google_compute_network.vpc.name
}