# VPC module #

This module creates a new VPC without any default subnets. The project ID is used to create the VPC name.

## Variables ##

### Required ###

- Project ID - string
- Region - string

## Outputs ##

- VPC name - string
