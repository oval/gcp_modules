resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  project                 = var.project
  auto_create_subnetworks = "false"
}

