output "external_ip" {
  description = "the external IP address"
  value       = google_compute_global_address.default.address
}

output "bucket_url" {
  value = google_storage_bucket.cdn_bucket.url
}