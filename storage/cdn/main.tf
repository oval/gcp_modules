module "name_gen" {
  source = "../../random_name"
  upper  = false
}

#--------------------------------------------------------------------------------------------------
# Setup bucket for cdn access
#--------------------------------------------------------------------------------------------------

resource "google_compute_backend_bucket" "default" {
  project     = var.project
  name        = module.name_gen.name
  bucket_name = var.bucket_name
  enable_cdn  = true
}

#--------------------------------------------------------------------------------------------------
# Create URL map
#--------------------------------------------------------------------------------------------------

resource "google_compute_url_map" "default" {
  project         = var.project
  name            = "${var.name}-default-map"
  default_service = google_compute_backend_bucket.default.self_link
}

resource "google_compute_url_map" "redirect" {
  project = var.project
  name    = "${var.name}-redirect-map"
  default_url_redirect {
    https_redirect = true
    strip_query    = false
  }
}

#--------------------------------------------------------------------------------------------------
# Create HTTP / HTTPS Proxy
#--------------------------------------------------------------------------------------------------

resource "google_compute_target_https_proxy" "default" {
  project          = var.project
  name             = "${var.name}-https-proxy"
  url_map          = google_compute_url_map.default.id
  ssl_certificates = [google_compute_managed_ssl_certificate.website.self_link]
}

resource "google_compute_target_http_proxy" "default" {
  project = var.project
  name    = "${var.name}-http-proxy-redirect"
  url_map = google_compute_url_map.redirect.id
}

#--------------------------------------------------------------------------------------------------
# Create a global public IP address if required
#--------------------------------------------------------------------------------------------------

resource "google_compute_global_address" "default" {
  project      = var.project
  name         = "${var.name}-address"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}

#--------------------------------------------------------------------------------------------------
# Create a SSL certificate if required
#--------------------------------------------------------------------------------------------------

resource "google_dns_managed_zone" "default" {
  project  = var.project
  name     = "${var.name}-dns"
  dns_name = var.dns_name
  labels   = {
    project     = var.project_label
    environment = var.environment_label
  }
}

resource "google_dns_record_set" "website" {
  project      = var.project
  name         = google_dns_managed_zone.default.dns_name
  type         = "A"
  ttl          = 300
  managed_zone = google_dns_managed_zone.default.name
  rrdatas      = [google_compute_global_address.default.address]
}

resource "google_compute_managed_ssl_certificate" "website" {
  project  = var.project
  provider = google-beta
  name     = "${var.name}-website-cert"
  managed {
    domains = [google_dns_record_set.website.name]
  }
}

#--------------------------------------------------------------------------------------------------
# Create a global forward rule
#--------------------------------------------------------------------------------------------------

resource "google_compute_global_forwarding_rule" "http" {
  project    = var.project
  name       = "${var.name}-http"
  target     = google_compute_target_http_proxy.default.self_link
  port_range = "80"
  ip_address = google_compute_global_address.default.address
  depends_on = [google_compute_global_address.default]
}


resource "google_compute_global_forwarding_rule" "https" {
  project    = var.project
  name       = "${var.name}-https"
  target     = google_compute_target_https_proxy.default.self_link
  port_range = "443"
  ip_address = google_compute_global_address.default.address
  depends_on = [google_compute_global_address.default]
}

#--------------------------------------------------------------------------------------------------
# Make the bucket public
#--------------------------------------------------------------------------------------------------

resource "google_storage_bucket_iam_member" "all_users_viewers" {
  bucket  = var.bucket_name
  role    = "roles/storage.objectViewer"
  member  = "allUsers"
}