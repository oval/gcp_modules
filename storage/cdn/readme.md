# Storage backend bucket #

This module is used to create a storage bucket with CDN access. The CDN accepts both HTTP and HTTPS access with the HTTP route immediately redirected to the HTTPS route.

## Variables ##

### Required ###

- Name - string
- Project - string
- Project label - string
- Environment label - string
- Bucket name - string this bucket must have a storage class of MULTI_REGIONAL

### Optional ###

- Force destroy - bool - default = true

## Outputs ##

- Bucket URL
- External IP
