#--------------------------------------------------------------------------------------------------
# Required
#--------------------------------------------------------------------------------------------------

variable "name" {
  description = "project name prefix for load balancing resources"
}

variable "project" {
  description = "Project ID to associate the CDN with"
}

variable "bucket_name" {
  description = "The bucket name to use as the base for the CDN. THis bucket needs to have the storage_class set to MULTI_REGIONAL"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}

variable "dns_name" {
  description = "A DNS name for the CDN, a SSL certificate will be created for this DNS name"
}

#--------------------------------------------------------------------------------------------------
# Optional
#--------------------------------------------------------------------------------------------------

variable "force_destroy" {
  default     = true
  description = "Should be bucket be forcfully destroyed, default to true"
  type        = bool
}
