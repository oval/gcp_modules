#--------------------------------------------------------------------------------------------------
# Required
#--------------------------------------------------------------------------------------------------

variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}

variable "dns_name" {
  description = "A DNS name for the CDN, a SSL certificate will be created for this DNS name"
}

#--------------------------------------------------------------------------------------------------
# Optional
#--------------------------------------------------------------------------------------------------

variable "force_destroy" {
  default     = true
  description = "Should be bucket be forcfully destroyed, default to true"
  type        = bool
}

variable "region" {
  default     = "EU"
  description = "The region the bucket will reside in, the default is set to EU. This is a MULTI_REGIONAL bucket and only uses the multi region types [US, EU, ASIA]"
  type        = string

  validation {
    # regex(...) fails if it cannot find a match
    condition     = can(regex("(US|EU|ASIA)", var.region))
    error_message = "The region value must be one of \"US\", \"EU\", \"ASIA\"."
  }
}

variable "uniform_bucket_level_access" {
  default     = true
  description = "does this bucket have uniform bucket level access"
  type        = bool
}

variable "index_page" {
  default     = "index.html"
  description = "The deault page to display for requests, defaults to 'index.html'"
}

variable "error_page" {
  default     = "index.html"
  description = "The deault page to display for page not found requests, defaults to 'index.html'"
}

variable "enable_cors" {
  default = false
  description = "boolean value whether or not to enable a specific cors policy. If this is set the 'true' then the variables [cors_origins, cors_methods, cors_extra_headers, cors_max_age_seconds] are required, defaults to 'false'"
}

variable "cors_origins" {
  default     = "*"
  description = "the CORS origins policy to apply, default is '*'"
}

variable "cors_methods" {
  default     = "*"
  description = "the CORS methods policy to apply, default is '*'"
}

variable "cors_extra_headers" {
  default     = "*"
  description = "the CORS headers policy to apply, default is '*'"
}

variable "cors_max_age_seconds" {
  default     = 3600
  description = "the CORS max age policy to apply, defaults to '3600'"
}

variable "access_logs_expiration_time_in_days" {
  default     = 30
  description = "how long should access logs be retained, defaults to 30 days"
}