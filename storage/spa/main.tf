module "name_gen" {
  source = "../../random_name"
  upper  = false
}

locals {
  website_domain_name_dashed = replace(var.dns_name, "/[._]/", "-")
}

resource "google_storage_bucket" "bucket" {
  name                        = module.name_gen.name
  location                    = var.region
  force_destroy               = var.force_destroy
  uniform_bucket_level_access = true
  project                     = var.project
  storage_class               = "MULTI_REGIONAL"

  website {
    main_page_suffix = var.index_page
    not_found_page   = var.error_page
  }

  dynamic "cors" {
    for_each = var.enable_cors ? ["cors"] : []
    content {
      origin          = var.cors_origins
      method          = var.cors_methods
      response_header = var.cors_extra_headers
      max_age_seconds = var.cors_max_age_seconds
    }
  }
  
  labels = {
    project     = var.project_label
    environment = var.environment_label
  }

  logging {
    log_bucket        = google_storage_bucket.access_logs.name
    log_object_prefix = "access-log"
  }
}

# -------------------------------------------------------------------------------------------------
# CREATE A SEPARATE BUCKET TO STORE ACCESS LOGS
# -------------------------------------------------------------------------------------------------

resource "google_storage_bucket" "access_logs" {
  provider = google-beta

  project = var.project

  # Use the dashed domain name
  name          = "${local.website_domain_name_dashed}-logs"
  location      = var.region
  storage_class = "MULTI_REGIONAL"

  force_destroy = var.force_destroy

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = var.access_logs_expiration_time_in_days
    }
  }
  labels = {
    project     = var.project_label
    environment = var.environment_label
  }
}