# Storage bucket #

This module is used to create a storage bucket

## Variables ##

### Required ###

- Project - string
- Project label - string
- Environment label - string
- DNS name - string

### Optional ###

- Force destroy - bool - default = true
- Region - string - default = "EU", must be one of [US, EU, ASIA]
- Uniform bucket level access - bool - default = true
- Index page - string - default = index.html
- Error page - string - default = index.html
- Enable cors - boolean - default = false
- CORS origins - string - default = *, only required if Enable cors = true
- CORS methods - string - default = *, only required if Enable cors = true
- CORS extra headers - string - default = *, only required if Enable cors = true
- CORS max age seconds - number - default = 3600, only required if Enable cors = true

## Outputs ##

- Bucket Name
- Bucket URL
