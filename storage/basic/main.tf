module "name_gen" {
  source = "../../random_name"
  upper  = false
}

resource "google_storage_bucket" "bucket" {
  name          = module.name_gen.name
  location      = var.region
  force_destroy = var.force_destroy

  uniform_bucket_level_access = true
  project = var.project
  labels  = {
    project     = var.project_label
    environment = var.environment_label
  }
}