variable "force_destroy" {
  default     = true
  description = "Should be bucket be forcfully destroyed, default to true"
  type        = bool
}

variable "region" {
  default     = "EUROPE-WEST2"
  description = "The region the bucket will reside in, the default is set to EUROPE-WEST2"
  type        = string
}

variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}

variable "uniform_bucket_level_access" {
  default     = true
  description = "does this bucket have uniform bucket level access"
  type        = bool
}