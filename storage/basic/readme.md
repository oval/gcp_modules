# Storage bucket #

This module is used to create a storage bucket

## Variables ##

### Required ###

- Project - string
- Project label - string
- Environment label - string

### Optional ###

- Force destroy - bool - default = true
- Region - string - default = "EUROPE-WEST2"
- Uniform bucket level access - bool - default = true

## Outputs ##

- Bucket Name
- Bucket URL
