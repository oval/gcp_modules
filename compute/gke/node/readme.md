# GKE Node module #

This module creates a node group as part of a given cluster.

The optional name variable is available for providing specific names to node pools, if none if provided then a randomised name is generated.

## Variables ##

### Required ###

- Cluster name - string
- Environment label - string
- Project - string
- Project label - string
- Service account - string

### Optional ###

- disk_size_gb - number - Default = 30
- gke_num_nodes - number - Default = 3
- machine_type - string - Default = n1-standard-1
- name - string - Default = ""
- preemptible - bool - Default = false
