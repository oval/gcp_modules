variable "cluster_name" {
  description = "Cluster name"
  type        = string
}

variable "disk_size_gb" {
  default     = 30
  description = "Disk size for the primary nodes in GB, min is 10GB. Default = 30"
  type        = number
  validation {
    condition     = var.disk_size_gb > 9
    error_message = "Must be a minimum size of 10."
  }
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}

variable "gke_num_nodes" {
  default     = 3
  description = "Number of gke nodes"
  type        = number
}

variable "machine_type" {
  default     = "n1-standard-1"
  description = "The primary node cluster machine type."
  type        = string
}

variable "name" {
  default     = ""
  description = "A specific name to give to the node pool"
  type        = string
}

variable "preemptible" {
  default     = false
  description = "Is this node preemptible? Preemptible node are not expected to last longer than 24h and are shutdown ungracefully Preemptible VMs are priced lower than standard Compute Engine VMs and offer the same machine types and options. For better availability, use smaller machine types."
  type        = bool
}

variable "project" {
  description = "Project ID"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "region" {
  description = "The region to deploy the resource"
  type        = string
}

variable "service_account" {
  description = "Service account for the node"
  type        = string
}
