module "node_name_gen" {
  source = "../../../random_name"
  upper  = false
}

locals {
  pool_name = length(var.name)>1 ? var.name : module.node_name_gen.name
}

data "google_compute_zones" "available" {
  region  = var.region
  project = var.project
}

resource "google_container_node_pool" "default" {
  name       = local.pool_name
  location   = data.google_compute_zones.available.names[0]
  cluster    = var.cluster_name
  node_count = var.gke_num_nodes
  project    = var.project

  node_config {
    disk_size_gb    = var.disk_size_gb
    machine_type    = var.machine_type
    oauth_scopes    = ["https://www.googleapis.com/auth/cloud-platform"]
    preemptible     = var.preemptible
    service_account = var.service_account
    tags            = ["gke-node", var.cluster_name]

    metadata = {
      disable-legacy-endpoints = "true"
      node_pool_id             = local.pool_name
    }
    
    labels = {
      project      = var.project_label
      environment  = var.environment_label
      node_pool_id = local.pool_name
    }

    workload_metadata_config {
      node_metadata = "GKE_METADATA_SERVER"
    }
  }
}