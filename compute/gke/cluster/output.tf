output "cluster_name" {
  value = google_container_cluster.primary.name
}

output "endpoint" {
  value = google_container_cluster.primary.endpoint
}

output "workload_identity_namespace" {
  value = "${var.project}.svc.id.goog"
}