module "cluster_name_gen" {
  source = "../../../random_name"
  upper  = false
}

data "google_compute_zones" "available" {
  region  = var.region
  project = var.project
}

resource "google_container_cluster" "primary" {
  name       = module.cluster_name_gen.name
  location   = data.google_compute_zones.available.names[0]
  project    = var.project
  network    = var.network
  subnetwork = var.subnetwork
  
  remove_default_node_pool = true
  initial_node_count       = var.initial_node_count

  release_channel {
    channel = "STABLE"
  }

  workload_identity_config {
    identity_namespace = "${var.project}.svc.id.goog"
  }
}