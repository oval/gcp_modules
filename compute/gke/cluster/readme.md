# GKE Cluster module #

This module creates a GKE cluster without a default node pool.

## Variables ##

### Required ###

- Username - string
- Password - string
- Network - string
- Subnetwork - string
- Project - string
- Region - string

### Optional ###

- Initial node count - number - Default = 1
