variable "initial_node_count" {
  default     = 1
  description = "Initial node count. Default = 1"
  type        = number
}

variable "network" {
  description = "VPC network"
  type        = string
}

variable "project" {
  description = "Project ID"
  type        = string
}

variable "region" {
  description = "Region for the cluster and nodes"
  type        = string
}

variable "subnetwork" {
  description = "VPC subnetwork"
  type        = string
}
