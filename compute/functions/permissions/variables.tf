variable "function_name" {
  description = "A name for the function"
  type        = string
}

variable "region" {
  default     = "EUROPE-WEST2"
  description = "The region the function will reside in, the default is set to EUROPE-WEST2"
  type        = string
}

variable "project" {
  description = "The project that this function will belong to"
  type        = string
}

variable "service_account_email" {
  description = "The service account to use with the function"
  type        = string
}