resource "google_cloudfunctions_function_iam_binding" "invoker" {
  project        = var.project
  region         = var.region
  cloud_function = var.function_name

  role   = "roles/cloudfunctions.invoker"
  members = [
    "serviceAccount:${var.service_account_email}"
  ]
}