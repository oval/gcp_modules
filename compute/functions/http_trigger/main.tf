resource "google_cloudfunctions_function" "function" {
  project               = var.project
  name                  = var.function_name
  description           = var.function_description
  runtime               = var.runtime
  timeout               = var.timeout
  trigger_http          = true
  entry_point           = var.entry_point
  available_memory_mb   = var.memory
  source_archive_bucket = var.bucket_name
  source_archive_object = var.bucket_key
  service_account_email = var.service_account_email
  environment_variables = var.environment_variables
  
  labels = {
    project = var.project_label
    environment = var.environment_label
  }
}