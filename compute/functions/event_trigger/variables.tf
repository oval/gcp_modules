variable "function_name" {
  description = "A name for the function"
  type        = string
}

variable "function_description" {
  description = "A description of the function"
  type        = string
}

variable "runtime" {
  description = "The function runtime"
  type        = string

  validation {
    condition     = can(regex("(nodejs10|nodejs12|nodejs14|python37|python38|python39|go111|go113|java11|dotnet3|ruby26)", var.runtime))
    error_message = "The region value must be one of \"nodejs10\", \"nodejs12\", \"nodejs14\", \"python37\", \"python38\", \"python39\", \"go111\", \"go113\", \"java11\", \"dotnet3\", \"ruby26\"."
  }
}

variable "memory" {
  default     = 128
  description = "The available memory. Default = 128"
  type        = number

  validation {
    condition     = can(regex("(128|256|512|1024|2048|4096)", var.runtime))
    error_message = "The region value must be one of \"128\", \"256\", \"512\", \"1024\", \"2048\", \"4096\"."
  }
}

variable "bucket_name" {
  description = "The storage bucket where the code is stored"
  type        = string
}

variable "bucket_key" {
  description = "The storage object key"
  type        = string
}

variable "entry_point" {
  description = "The function entry point"
  type        = string
}

variable "service_account_email" {
  description = "The service account to use with the function"
  type        = string
}

variable "environment_variables" {
  description = "A set of key/pair environment variables"
  type        = set(string)
}

variable "timeout" {
  default     = 30
  description = "A max execution time (seconds) for the function. default = 30"
  type        = number
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}

variable "event_type" {
  description = "The event type the function will respond to"
  type        = string
}

variable "event_resource" {
  description = "The resource that the event will listen to"
  type        = string
}