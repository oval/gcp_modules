output "subscription_id" {
  value = google_pubsub_subscription.default.id
}

output "subscription_path" {
  value = google_pubsub_subscription.default.path
}