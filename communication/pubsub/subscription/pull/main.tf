module "name_gen" {
  source = "../../../../random_name"
  upper  = false
}

resource "google_pubsub_subscription" "default" {
  name                    = module.name_gen.name
  topic                   = var.topic_id
  project                 = var.project
  enable_message_ordering = var.fifo
}