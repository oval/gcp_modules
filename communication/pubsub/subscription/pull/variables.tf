variable "project" {
  description = "The project to associate the role with"
}

variable "topic_id" {
  description = "The topic id to subscribe to"
  type        = string
}

variable "fifo" {
  description = "Is this subscription an ordered list subscription?"
  default     = false
}