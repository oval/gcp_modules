# Pub/Sub Subscription #

This module is used to create a pull based subscription to an existing Pub/Sub topic

## Variables ##

### Required ###

- Topic id

### Optionals ###

- FIFO - boolean - default = false

## Outputs ##

- Subscription id
- Subscription path
