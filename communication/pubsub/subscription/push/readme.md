# Pub/Sub Subscription #

This module is used to create a push based subscription to an existing Pub/Sub topic

## Variables ##

### Required ###

- Topic id
- Project label
- Environment label
- Push endpoint

## Outputs ##

- Subscription id
- Subscription path
