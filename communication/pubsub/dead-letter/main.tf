module "name_gen" {
  source = "../../../random_name"
  upper  = false
}

module "dead_letter_topic" {
  source            = "../topic"
  project           = var.project
  environment_label = var.environment_label
  project_label     = var.project_label
}

resource "google_pubsub_subscription" "default" {
  name  = module.name_gen.name
  topic = var.topic_id

  dead_letter_policy {
    dead_letter_topic = module.dead_letter_topic.id
    max_delivery_attempts = var.max_delivery_attempts
  }
}