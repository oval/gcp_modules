output "dead_letter_topic_id" {
  value = module.dead_letter_topic.id
}

output "dead_letter_subscription_id" {
  value = google_pubsub_subscription.default.id
}

output "dead_letter_subscription_path" {
  value = google_pubsub_subscription.default.path
}