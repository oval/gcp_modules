# Pub/Sub Dead letter topic #

This module is used to create a dead letter topic and subscription to an existing Pub/Sub topic

## Variables ##

### Required ###

- Primary topic id
- Project id
- Project label
- Environment label

### Optional ###

- Max delivery attempts - number - default = 10

## Outputs ##

- Dead letter topic id
- Dead letter subscription id
- Dead letter subscription path
