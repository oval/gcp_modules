#--------------------------------------------------------------------------------------------------
# Required
#--------------------------------------------------------------------------------------------------

variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the topic and subscription"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the topic and subscription"
  type        = string
}

variable "topic_id" {
  description = "the id of the primary topic that feeds the deal letter topic"
  type        = string
}

#--------------------------------------------------------------------------------------------------
# Optional
#--------------------------------------------------------------------------------------------------

variable "max_delivery_attempts" {
  default     = 10
  description = "the maximum number of attempts the primary topic should make before adding items to the deal letter queue"
}