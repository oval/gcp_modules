variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "project_label" {
  description = "The project label to associate with the bucket"
  type        = string
}

variable "environment_label" {
  description = "The environment label to associate with the bucket"
  type        = string
}