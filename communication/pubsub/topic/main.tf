module "name_gen" {
  source = "../../../random_name"
  upper  = false
}

resource "google_pubsub_topic" "default" {
  project = var.project
  name    = module.name_gen.name
  labels  = {
    project     = var.project_label
    environment = var.environment_label
  }
}