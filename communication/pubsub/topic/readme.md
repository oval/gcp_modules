# Pub/Sub Topic #

This module is used to create a pub/sub topic in the specified project.

## Variables ##

### Required ###

- Project - string
- Project label - string
- Environment label - string

## Outputs ##

- Topic id
