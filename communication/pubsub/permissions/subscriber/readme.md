# Pub/Sub subscriber permissions #

This module is used to add the pub/sub subscriber permissions to the provided service account.

## Variables ##

### Required ###

- Project - string
- Subscription id - string
- Service account email - string

## Outputs ##
