resource "google_pubsub_subscription_iam_binding" "binding" {
  project = var.project
  subscription  = var.subscription_id
  role = "roles/pubsub.subscriber"
  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}