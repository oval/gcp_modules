variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "topic_id" {
  description = "Id of the topic to create the permission for"
  type        = string
}

variable "service_account_email" {
  description = "The email address for the designated service account"
  type        = string
}