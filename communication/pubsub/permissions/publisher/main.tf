resource "google_pubsub_topic_iam_binding" "binding" {
  project = var.project
  topic = var.topic_id
  role = "roles/pubsub.publisher"
  members = [
    "serviceAccount:${var.service_account_email}",
  ]
}