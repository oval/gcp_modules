# Pub/Sub publisher permissions #

This module is used to add the pub/sub topic publisher permissions to the provided service account.

## Variables ##

### Required ###

- Project - string
- Topic id - string
- Service account email - string

## Outputs ##
