variable "service_account_email" {
  description = "The service account email that the role will be bound to"
  type        = string
}

variable "role" {
  description = "The role to bind"
  type        = string
}

variable "project" {
  description = "The project ID"
  type        = string
}