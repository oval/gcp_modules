resource "google_storage_bucket_access_control" "public_rule" {
  bucket = var.bucket_name
  role   = "READER"
  entity = "allUsers"
}