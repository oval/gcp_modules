# Public readonly Bucket policy #

This module adds a readonly bucket access policy and returns the policy id

## Variables ##

### Required ###

- Bucket name

## Output ##

- ID
