variable "bucket_name" {
  description = "The bucket to apply to the binding to"
  type        = string
}

variable "role" {
  description = "The role to bind the accout to"
  type        = string
}

variable "service_account_email" {
  description = "The service account email to bind to the bucket and role"
  type        = string
}