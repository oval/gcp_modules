# Service account #

This module is used to create a new Service account

## Variables ##

### Required ###

- Project id
- Account id

## Outputs ##

- Service account id
- Service account email
- Service account name
