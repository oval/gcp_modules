module "name_gen" {
  source = "../../random_name"
  upper  = false
  name_length = 28
}
resource "google_service_account" "service_account" {
  account_id   = module.name_gen.name
  display_name = module.name_gen.name
  project      = var.project
}