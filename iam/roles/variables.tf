# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED MODULE PARAMETERS
# These parameters must be supplied when consuming this module.
# ---------------------------------------------------------------------------------------------------------------------

variable "title" {
  description = "A title for the role"
  type        = string
}

variable "description" {
  description = "A description of the role and what is is used for"
  type        = string
}

variable "project" {
  description = "The project to associate the role with"
}

variable "permissions" {
  description = "A list of the permissions to attach to this role"
  type = list(string)
}