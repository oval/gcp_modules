# IAM Role #

This module is used to create an IAM role with a randomised ID

## Variables ##

### Required ###

- Project - string
- Name - string
- Permissions - list of string
- Title - string
- Description - string

## Outputs ##

- Role ID
- Role Name
