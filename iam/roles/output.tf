output "role_id" {
  value = google_project_iam_custom_role.role.id
}

output "role_name" {
  value = google_project_iam_custom_role.role.name
}