module "name_gen" {
  source = "../../random_name"
}

resource "google_project_iam_custom_role" "role" {
  role_id     = module.name_gen.name
  title       = var.title
  description = var.description
  permissions = var.permissions
  project = var.project
}