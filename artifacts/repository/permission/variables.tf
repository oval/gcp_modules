variable "project" {
  description = "The project that this bucket will belong to"
  type        = string
}

variable "region" {
  description = "THe location of the repository"
  type        = string
}

variable "repository_name" {
  description = "The repository name you are granting pull permission to"
  type        = string
}

variable "role" {
  description = "The role that is going to be applied to the service account"
}

variable "service_account_email" {
  description = "The service account email address that needs permission to pull images"
  type        = string
}