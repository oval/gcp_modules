resource "google_artifact_registry_repository_iam_binding" "binding" {
  provider = google-beta

  project    = var.project
  location   = var.region
  repository = var.repository_name
  role       = var.role
  members    = [
    "serviceAccount:${var.service_account_email}",
  ]
}