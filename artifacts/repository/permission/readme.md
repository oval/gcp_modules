# GCR permissions module

This module is designed to provide a given role for the specified repository.

## Variables

- Project id - string
- Region - string
- Repository name - string
- Role - string
- Service account email - string
