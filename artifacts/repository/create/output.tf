output "repository_id" {
  value = google_artifact_registry_repository.repository.id
}

output "repository_name" {
  value = google_artifact_registry_repository.repository.name
  description = "the long name of the repository, for example: 'projects/p1/locations/us-central1/repositories/repo1'"
}