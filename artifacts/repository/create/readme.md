# Repository creation module

This modules create a new repository for maintaining private docker images in GCR

## Variables

- Project name - string
- Region - string
- Repository name - string
- Description - string

## Output

- Repository id
- Repository name
