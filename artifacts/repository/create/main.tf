resource "google_artifact_registry_repository" "repository" {
  provider = google-beta

  project       = var.project
  location      = var.region
  repository_id = var.repository_name
  description   = var.description
  format        = "DOCKER"
}