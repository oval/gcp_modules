variable "project" {
  description = "The project that this repository will belong to"
  type        = string
}

variable "region" {
  description = "The location of the repository"
  type        = string
}

variable "repository_name" {
  description = "A unique name for the new repository"
  type        = string
}

variable "description" {
  description = "The repository description"
  type        = string
}
