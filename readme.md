# GCP core terraform modules #

These modules can be linked in terraform projects using the module syntax:

```Terraform
module "name" {
    source = "bitbucket.org/oval/gcp_modules//path/to/my_module?ref=v0.0.1"

    ...
}
```

This allows generic reuse without the need to copy/paste or fork repositories. The *my_module* would be replaced with the sub module you require i.e. *vpc* and the *ref* version is used to differentiate between developmental changes dor DEV/UAT and Production branches. A dev update may create a new tag v0.0.2 and this can be applied to UAT but production would still run v0.0.1 until the next version is complete.

## Updating the modules ##

In order to update or add a module the following git commands are necessary to create a tagged branch for another library to pull

```BASH
git commit ...
git tag -a 'version number' -m 'your tag message'
git push origin branch --follow-tags
```
