Param(
  [Parameter(Mandatory=$True)]
  [String]
  $Message,
  [Switch]
  $Major,
  [Switch]
  $Minor,
  [Switch]
  $Trim
)

#--------------------------------------------------------------------------------------------------
# Helper functions
#--------------------------------------------------------------------------------------------------

# Checks tag and returns the Patch version number.
# Patches are minor non breaking changes
function GetPatch {
  Param([String] $Str)

  $lastDot = $Str.LastIndexOf(".");
  if( $lastDot -ge 1 ) {
    $Str.SubString($lastDot+1)
  }
}

# Checks tag and returns the Minor version number.
# Minor changes are new features that do not affect backward compatability
function GetMinor {
  Param([String] $Str)

  $FirstDot = $Str.IndexOf(".") + 1
  $lastDot = $Str.LastIndexOf(".")
  $MinorLength = $LastDot-$FirstDot
  if( $MinorLength -ge 1 ) {
    $Str.SubString($FirstDot, $MinorLength)
  }  
  else {
    $Str.SubString($FirstDot, 1)
  }  
}  

# Checks tag and returns the Major version number.
# Major changes are breaking changes that affect backwards compatability
function GetMajor {
  Param([String] $Str)

  $FirstDot = $Str.IndexOf(".")
  $MajorLength = $FirstDot-1
  if( $MajorLength -ge 1 ) {
    $Str.SubString(1, $MajorLength)
  }  
  else {
    $Str.SubString(1, 1)
  }  
}  

#--------------------------------------------------------------------------------------------------
# Execution code
#--------------------------------------------------------------------------------------------------

# Quietly check all the tags are available
git fetch --all --tags --quiet

# Iterate tag list and map to objects for sorting
$tagList = git tag --list | Foreach-Object {
  if($_ -match '^v\d+\.\d+\.\d+$') {
    $ma = GetMajor $_
    $mi = GetMinor $_
    $pa = GetPatch $_
    [PSCustomObject]@{
      Major = [int]$ma
      Minor = [int]$mi
      Patch = [int]$pa
    }
  }
}

$ascendingList = $tagList | Sort-Object -Property @{Expression = "Major"; Descending = $True}, @{Expression = "Minor"; Descending = $True}, @{Expression = "Patch"; Descending = $True}

# Get most recent tag version
$Tag = [PSCustomObject]@{
  Major = $ascendingList[0].Major
  Minor = $ascendingList[0].Minor
  Patch = $ascendingList[0].Patch
}

# Apply revision update
if($Major) {
  $Tag.Major = $Tag.Major + 1
  $Tag.Minor = 0
  $Tag.Patch = 0
}
elseif($Minor) {
  $Tag.Minor = $Tag.Minor + 1
  $Tag.Patch = 0
}
else {
  $Tag.Patch = $Tag.Patch + 1
}

# Build tag string
$a = $Tag.Major
$b = $Tag.Minor
$c = $Tag.Patch
$t = "v$a.$b.$c"

# Commit, tag and push to remote
git commit -m $Message
if($?) {
  git tag -a $t -m $Message
  git push origin master --follow-tags
}

# Used to remove all but the most recent 5 versioned tags
if($Trim) {
  $descendingList = $tagList | Sort-Object -Property @{Expression = "Major"; Descending = $False}, @{Expression = "Minor"; Descending = $False}, @{Expression = "Patch"; Descending = $False}
  $descendingList[0..($descendingList.Count-5)] | Foreach-Object {
    $a = $_.Major
    $b = $_.Minor
    $c = $_.Patch
    git tag -d "v$a.$b.$c" --quiet
  }
}