terraform {
  required_providers {
    random = {
      version = "~> 3.0.0"
    }
  }
}

resource "random_string" "prefix" {
  length  = 2
  lower   = true
  number  = false
  upper   = false
  special = false
}

resource "random_string" "body" {
  length  = var.name_length - 2
  lower   = var.lower
  number  = var.number
  upper   = var.upper
  special = var.special
}