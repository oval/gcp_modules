variable "name_length" {
  description = "how long should be string be?"
  default     = 30
  type        = number
  validation {
    condition     = var.name_length > 2
    error_message = "Must be a minimum length of 3."
  }
}

variable "upper" {
  description = "To contain uppercase letters?"
  default     = true
  type        = bool
}

variable "number" {
  description = "To contain numbers?"
  default     = true
  type        = bool
}

variable "lower" {
  description = "To contain lowercase letters?"
  default     = true
  type        = bool
}

variable "special" {
  description = "To contain special characters?"
  default     = false
  type        = bool
}