# Random name generator #

Used to generate random names. all names begin with 2 random lowercase letters. These are resource names not human names.

## Variables ##

### Optional ###

- length - number - Default = 30
- uppercase - boolean - Default = true
- lowercase - boolean - Default = true
- numbers - boolean - Default = true
- special - boolean - Default = false
