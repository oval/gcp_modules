#
# This will delete all but the 5 most recent tags
#

function GetPatch {
  Param( 
    [String]
    $Str
  )
  $lastDot = $Str.LastIndexOf(".");
  if( $lastDot -ge 1 ) {
    $Str.SubString($lastDot+1)
  }
}

function GetMinor {
  Param( 
    [String]
    $Str
  )  
  $FirstDot = $Str.IndexOf(".") + 1
  $lastDot = $Str.LastIndexOf(".")
  $MinorLength = $LastDot-$FirstDot
  if( $MinorLength -ge 1 ) {
    $Str.SubString($FirstDot, $MinorLength)
  }  
  else {
    $Str.SubString($FirstDot, 1)
  }  
}  

function GetMajor {
  Param( 
    [String]
    $Str
  )  
  $FirstDot = $Str.IndexOf(".")
  $MajorLength = $FirstDot-1
  if( $MajorLength -ge 1 ) {
    $Str.SubString(1, $MajorLength)
  }  
  else {
    $Str.SubString(1, 1)
  }  
}  

$tagList = git tag --list | Foreach-Object {
  $Major = GetMajor $_
  $Minor = GetMinor $_
  $Patch = GetPatch $_
  [PSCustomObject]@{
    Major = [int]$Major
    Minor = [int]$Minor
    Patch = [int]$Patch
  }
} | Sort-Object -Property @{Expression = "Major"; Descending = $False}, @{Expression = "Minor"; Descending = $False}, @{Expression = "Patch"; Descending = $False}

$tagList[0..($tagList.Count-5)] | Foreach-Object {
  $ma = $_.Major
  $mi = $_.Minor
  $pa = $_.Patch
  git tag -d "v$ma.$mi.$pa"
}
